#include <Arduino.h>

template <byte leftEmitter, byte leftDetector, byte frontEmitter, byte frontDetector,  byte rightEmitter, byte rightDetector>

class NanoMouseSensors
{
  private:
    //Used to store sensor values when the emitters are off
    int leftAmbient;
    int frontAmbient;
    int rightAmbient;

    // Used to store sensors values when the emitters are on
    int leftCombined;
    int frontCombined;
    int rightCombined;

    // filters out light
    int leftReflected;
    int frontReflected;
    int rightReflected;

    //variables used for smoothing
    int leftTotal;
    int frontTotal;
    int rightTotal;

    static const byte numReadings = 20;
    byte index;

    int leftReadings[numReadings];
    int leftSmoothed;

    int frontReadings[numReadings];
    int frontSmoothed;


    int rightReadings[numReadings];
    int rightSmoothed;


  public:
    int left;
    int front;
    int right;
  
    void configure()
    {
      pinMode(leftEmitter, OUTPUT);
      pinMode(frontEmitter, OUTPUT);
      pinMode(rightEmitter, OUTPUT);
    }

    void sense()
    {
      digitalWrite(leftEmitter, HIGH);
      digitalWrite(frontEmitter, HIGH);
      digitalWrite(rightEmitter, HIGH);

      delay(1);

      leftCombined = analogRead(leftDetector);
      frontCombined = analogRead(frontDetector);
      rightCombined = analogRead(rightDetector);

      digitalWrite(leftEmitter, LOW);
      digitalWrite(frontEmitter, LOW);
      digitalWrite(rightEmitter, LOW);

      delay(1);

      leftAmbient = analogRead(leftDetector);
      frontAmbient = analogRead(frontDetector);
      rightAmbient = analogRead(rightDetector);

      leftReflected = leftCombined - leftAmbient;
      frontReflected = frontCombined - frontAmbient;
      rightReflected = rightCombined - rightAmbient;

      leftTotal -= leftReadings[index];
      leftReadings[index] = leftReflected;
      leftTotal += leftReadings[index];


      frontTotal -= frontReadings[index];
      frontReadings[index] = frontReflected;
      frontTotal += frontReadings[index];


      rightTotal -= rightReadings[index];
      rightReadings[index] = rightReflected;
      rightTotal += rightReadings[index];

      index += 1;
      if (index >= numReadings)
      {
        index = 0;
      }
      leftSmoothed = leftTotal / numReadings;
      frontSmoothed = frontTotal / numReadings;
      rightSmoothed = rightTotal / numReadings;

      left = leftSmoothed;
      front = frontSmoothed;
      right = rightSmoothed;
    }


    void view()
    {
      Serial.print(left);
      Serial.print("\t");
      Serial.print(front);
      Serial.print("\t");
      Serial.println(right);

    }
};

