#include <Servo.h>
#include "NanoMouseMotors.h"
#include "NanoMouseSensors.h"


const byte ledPin = 13;
const byte buttonPin = 9;

NanoMouseMotors motors;

//leftEmitter, leftDetector, frontEmitter...
NanoMouseSensors<2, A7, 3, A6, 4, A5> sensors;

void setup()
{
  motors.attach(6, 5);

  pinMode(ledPin, OUTPUT);
  pinMode(buttonPin, INPUT_PULLUP);

  sensors.configure();

  Serial.begin(9600);

  while (digitalRead(buttonPin))
  {
  }


}

void loop()
{
 avoid(state());
}

byte state()
{
  int threshold = 30;
  byte event = 0;

  sensors.sense();

  if (sensors.front > threshold)
    event += 1;

  if (sensors.left > threshold)
    event += 2;

  if (sensors.right > threshold)
    event += 4;

  return event;
}

void avoid(byte event)
{
  switch (event)
  {
    case 1: //front sensor triggered
      motors.turn(LEFT, 90);
      break;
    case 2: //LEFT sensor is triggered
      motors.turn(RIGHT, 45);
      break;
    case 4: //RIGHT sensors triggered
      motors.turn(LEFT, 45);
      break;
      default:
      motors.forward();
      break;


  }
}



